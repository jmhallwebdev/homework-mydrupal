<?php

	function spring_preprocess_page(&$variables){

		$page = $variables['page'];
		$sidebar_class_left = 'one-fourth';
		$sidebar_class_right = 'one-fourth';
		$content_class = 'one-half';
		$variables['page']['tpl_control'] = [];

		if (!empty($page['sidebar_left']) && !empty($page['sidebar_right']))
		{
			$sidebar_class_left = 'one-fourth';
			$sidebar_class_right = 'one-fourth';
			$content_class = 'one-half';
		}
		else if (empty($page['sidebar_left']) && !empty($page['sidebar_right']))
		{
			$sidebar_class_left = '';
			$sidebar_class_right = 'one-fourth';
			$content_class = 'three-fourths';
		}
		else if (!empty($page['sidebar_left']) && empty($page['sidebar_right']))
		{
			$sidebar_class_left = 'one-fourth';
			$sidebar_class_right = '';
			$content_class = 'three-fourths';
		}
		else
		{
			$content_class = 'full-width';
		}

		$variables['page']['tpl_control']['sidebar_class_left'] = $sidebar_class_left;
		$variables['page']['tpl_control']['sidebar_class_right'] = $sidebar_class_right;
		$variables['page']['tpl_control']['content_class'] = $content_class;
		
		/*
		if sidebar_left is occupied {
			add class of one-fourth to sidebar-left
			add class of three-fourths to content
		} 
		else {
			add class of full-width to content
			add class of '' to sidebar_left
		}
		*/
	}